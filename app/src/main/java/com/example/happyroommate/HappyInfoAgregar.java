package com.example.happyroommate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.happyroommate.dataBase.InfoLab;
import com.example.happyroommate.entity.Info;

import java.io.Serializable;

public class HappyInfoAgregar extends AppCompatActivity implements View.OnClickListener {

    TextView propietario, contenido;
    Button btnAdd;

    //Elementos para instanciar la BBDD
    private InfoLab mInfoLab;
    private Info mInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_happy_info_agregar);

        //Se instancia la BBDD
        mInfoLab = InfoLab.get(this);

        propietario = findViewById(R.id.nombreHi);
        contenido = findViewById(R.id.contenidoHI);

        btnAdd = findViewById(R.id.agregarComentario);
        btnAdd.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        String propietarioAdd = propietario.getText().toString();
        String contenidoAdd = contenido.getText().toString();

        if (propietarioAdd.isEmpty() || contenidoAdd.isEmpty()) {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Todos los campos deben estar llenos", Toast.LENGTH_LONG);
            toast.show();
        } else {

            Info info = new Info();
            info.setNombre(propietarioAdd);
            info.setContenido(contenidoAdd);
            info.setColor("#001f33");
            mInfoLab.addInfo(info);

            Toast toast = Toast.makeText(getApplicationContext(),
                    "Contenido agregado correctamente", Toast.LENGTH_LONG);
            toast.show();

            Intent intentHappyRoom = new Intent(this, HappyInfo.class);
            startActivity(intentHappyRoom);

        }

    }
}
