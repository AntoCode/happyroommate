package com.example.happyroommate.dataBase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.happyroommate.entity.Info;

@Database(entities = {Info.class}, version = 1)
public abstract class InfoDatabase extends RoomDatabase {
    public abstract InfoDao getInfoDao();
}
