package com.example.happyroommate.dataBase;

import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.happyroommate.entity.Info;

import java.util.List;

public class InfoLab {

    @SuppressLint("StaticFieldLeak")
    private static InfoLab sInfoLab;

    private InfoDao mInfoDao;

    private InfoLab(Context context) {
        Context appContext = context.getApplicationContext();
        InfoDatabase database = Room.databaseBuilder(appContext, InfoDatabase.class, "info")
                .allowMainThreadQueries().build();
        mInfoDao = database.getInfoDao();
    }

    public static InfoLab get(Context context) {
        if (sInfoLab == null) {
            sInfoLab = new InfoLab(context);
        }
        return sInfoLab;
    }


    public List<Info> getInfos() {
        return mInfoDao.getInfos();
    }

    public Info getInfo(String id) {
        return mInfoDao.getInfo(id);
    }

    public void addInfo(Info info) {
        mInfoDao.addInfo(info);
    }

    public void updateInfo(Info info) {
        mInfoDao.updateInfo(info);
    }

    public void deleteInfo(Info info) {
        mInfoDao.deleteInfo(info);
    }

}
