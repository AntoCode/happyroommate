package com.example.happyroommate.dataBase;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.happyroommate.entity.Info;

import java.util.List;

@Dao
public interface InfoDao {

    @Query("SELECT * FROM info")
    List<Info> getInfos();

    @Query("SELECT * FROM info WHERE id LIKE :uuid")
    Info getInfo(String uuid);

    @Insert
    void addInfo(Info book);

    @Delete
    void deleteInfo(Info book);

    @Update
    void updateInfo(Info book);
}
