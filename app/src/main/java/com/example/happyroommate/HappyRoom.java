package com.example.happyroommate;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.happyroommate.dataBase.InfoDao;
import com.example.happyroommate.dataBase.InfoLab;
import com.example.happyroommate.entity.Info;
import com.example.happyroommate.model.Propietario;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HappyRoom extends AppCompatActivity implements View.OnClickListener {

    EditText metrosCuadrados, numeroProp, costoMes, metrosPiso;
    TextView nombreP;
    Button calcular, agregarPropietario;
    List<Propietario> propietarioList;

    private InfoLab mInfoLab;
    private Info mInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_happy_room);

        Log.i("Main HappyRoom", "Entrando al Activity HappyRoom");

        //Capturando variables en las vistas
        calcular = findViewById(R.id.calcularHR);
        agregarPropietario = findViewById(R.id.agregarProp);

        calcular.setOnClickListener(this);
        agregarPropietario.setOnClickListener(this);

        propietarioList = new ArrayList<>();

        mInfoLab = InfoLab.get(this);

        List<Info> infos = mInfoLab.getInfos();

        Log.i("Main HappyRoom", "Entrnado al Activity HappyRoom");

    }

    /*
     * Como tenemos multiples botones
     * y un solo evento onClick podemos
     * verificar el id de cada boton con
     * el id del objeto View que
     * viene de entrada
     */
    @Override
    public void onClick(View v) {
        Log.i("Main HappyRoom", "Verificando si se debe calcular o agregar");

        metrosCuadrados = (EditText) findViewById(R.id.metrosHR);
        numeroProp = (EditText) findViewById(R.id.numerosPropHR);
        costoMes = (EditText) findViewById(R.id.costoMensual);
        metrosPiso = (EditText) findViewById(R.id.metrosPiso);
        int amount = 0;
        int numeroPropParse = 0;
        int costoMensual = 0;
        int metrosPisoTotal = 0;
        boolean check = true;

        /*
         * Verificamos que todos los
         * atributos sean correctos,
         * en caso de error lanzamos
         * una notificacion al usuario
         */
        try {
            amount = Integer.parseInt(metrosCuadrados.getText().toString());
        } catch (Exception e) {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Debe ingresar un numero valido para saber los metros cuadrados", Toast.LENGTH_LONG);
            toast.show();
        }

        try {
            numeroPropParse = Integer.parseInt(numeroProp.getText().toString());
        } catch (Exception e) {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Debe ingresar un numero valido para saber el numero de propietarios", Toast.LENGTH_LONG);
            toast.show();
            check = false;
        }

        try {
            costoMensual = Integer.parseInt(costoMes.getText().toString());
        } catch (Exception e) {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Debe ingresar un numero valido para saber los metros totales del piso", Toast.LENGTH_LONG);
            toast.show();
            check = false;
        }

        try {
            metrosPisoTotal = Integer.parseInt(metrosPiso.getText().toString());
        } catch (Exception e) {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Los metros del piso no son validos", Toast.LENGTH_LONG);
            toast.show();
            check = false;
        }

        nombreP = findViewById(R.id.titularHR);

        //Verificando que todos los datos sean correctos para poder agregar un propietario
        if (amount <= 0 || nombreP.getText().toString().isEmpty() || numeroPropParse <= 0) {
            Log.e("Error Main Boton", "Debe ingresar un nombre primero!");
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Verifique que todos los campos estan rellenados correctamente!", Toast.LENGTH_LONG);
            toast.show();
            check = false;
        }

        /*
         * Verificamos si debemos
         * agregar un usuario o calcular los costos
         */
        if (v.getId() == R.id.agregarProp) {
            Log.d("Main HappyRoom", "Se agrega propietario");

            String nombre = nombreP.getText().toString();
            //agregamos un propietario nuevo
            Propietario propietario = new Propietario();
            propietario.setMetrosCuadrados(amount);
            propietario.setNombre(nombre);

            if (propietarioList.size() < numeroPropParse && check) {
                propietarioList.add(propietario);
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Propietario Agregado Correctamente", Toast.LENGTH_LONG);
                toast.show();
            } else if (check) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Numero maximo de propietarios agregados!", Toast.LENGTH_LONG);
                toast.show();
            }

        } else {

            if (numeroPropParse > 0 && propietarioList.size() == numeroPropParse) {
                Log.d("Main HappyRoom", "Todos los usuarios agregados correctamente se envia la data a la siguiente Activity");
                //Navegamos al panel de control pasandole el nombre del usuario
                Intent intentHappyRoom = new Intent(this, HappyRoomResponse.class);
                //intentHappyRoom.putParcelableArrayListExtra("propietarioList", (ArrayList<Propietario>) propietarioList);
                intentHappyRoom.putExtra("propietarioList", (Serializable) propietarioList);
                intentHappyRoom.putExtra("costoMensual", costoMensual);
                intentHappyRoom.putExtra("metrosPiso", metrosPisoTotal);
                startActivity(intentHappyRoom);
            } else {
                Log.d("Main HappyRoom", "Faltan usuarios por agregar");
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Faltan usuarios por agregar!", Toast.LENGTH_LONG);
                toast.show();
            }
        }

    }
}
