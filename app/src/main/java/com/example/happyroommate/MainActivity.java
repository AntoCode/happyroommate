package com.example.happyroommate;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    TextView nombrePrincipal;
    Button btnPrincipal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
         * Obteniendo el nombre principal del usuario
         * al momento de darle click al boton "VAMOS"
         */

        nombrePrincipal = findViewById(R.id.nombrePersona);
        //capturando el boton principal
        btnPrincipal = findViewById(R.id.botonLogin);

        Log.d("Nombre del usuario ->", nombrePrincipal.toString());


        btnPrincipal.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Log.i("Main Boton", "Boton principal activado!");

        String nombreP = nombrePrincipal.getText().toString();

        if (!nombreP.isEmpty()) {
            Log.i(" Main Boton", "Valores correcto redireccionando al portal");


            //Navegamos al panel de control pasandole el nombre del usuario
            Intent intentPanelControl = new Intent(this, ControlPanel.class);
            intentPanelControl.putExtra("nombrePrincipal", nombreP);

            startActivity(intentPanelControl);


        } else {
            Log.e("Error Main Boton", "Debe ingresar un nombre primero!");
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Debe ingresar un nombre primero!", Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
