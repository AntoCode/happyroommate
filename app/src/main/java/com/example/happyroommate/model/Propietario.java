package com.example.happyroommate.model;

import java.io.Serializable;

/**
 * Clase propietario
 * se utiliza al momento
 * de agregar un propietario
 * para calcular el costo de la
 * vivienda por habitacion
 */
public class Propietario implements Serializable {


    private String nombre;
    private int metrosCuadrados;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getMetrosCuadrados() {
        return metrosCuadrados;
    }

    public void setMetrosCuadrados(int metrosCuadrados) {
        this.metrosCuadrados = metrosCuadrados;
    }

    public Propietario() {
    }

}
