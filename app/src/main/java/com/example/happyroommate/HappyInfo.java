package com.example.happyroommate;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.happyroommate.dataBase.InfoLab;
import com.example.happyroommate.entity.Info;

import java.util.ArrayList;
import java.util.List;


public class HappyInfo extends AppCompatActivity implements View.OnClickListener {


    ListView listaView;
    Button btnAdd;

    //Elementos para instanciar la BBDD
    private InfoLab mInfoLab;
    private Info mInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_happy_info);

        //Se instancia la BBDD
        mInfoLab = InfoLab.get(this);

        Log.d("HappyInfo", "Entrando en la Actividad correspondiente");

        listaView = findViewById(R.id.listaHI);

        //Data de la BBDD
        List<Info> infos = mInfoLab.getInfos();





        List<String> listaFormada = generateResponse(infos);

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, listaFormada);
        //pintamos la lista
        listaView.setAdapter(adaptador);

        //Redireccionando a la activity requerida
        btnAdd = findViewById(R.id.agregarHI);
        btnAdd.setOnClickListener(this);

    }


    public List<String> generateResponse(List<Info> infoList) {

        List<String> responseList = new ArrayList<>();
        String response;
        for (int i = 0; i < infoList.size(); i++) {
            Info info = infoList.get(i);
            response = "Mensaje de " + info.getNombre() + ": " + info.getContenido();
            responseList.add(response);
        }

        return responseList;
    }

    @Override
    public void onClick(View v) {
        Log.i("HappyInfo", "Navegando a Happy Info Agregar");
        //Navegamos al panel de control pasandole el nombre del usuario
        Intent intentHappyInfo = new Intent(this, HappyInfoAgregar.class);
        startActivity(intentHappyInfo);
    }
}
