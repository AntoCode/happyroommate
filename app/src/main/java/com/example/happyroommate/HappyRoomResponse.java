package com.example.happyroommate;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.happyroommate.model.Propietario;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que da una respuesta de todos los propietarios
 * con sus costes por habitacion
 */
public class HappyRoomResponse extends AppCompatActivity {


    ListView listaView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_happy_room_response);

        Bundle extrasIntent = getIntent().getExtras();

        List<Propietario> propietarioList = (List<Propietario>) extrasIntent.getSerializable("propietarioList");

        int costoMensual = extrasIntent.getInt("costoMensual");
        int metrosPiso = extrasIntent.getInt("metrosPiso");


        Log.d("HappyRoomResponse", "Lista obtenida correctamente");

        listaView = findViewById(R.id.listaCostoPisos);

        //Calculamos el precio de las habitaciones
        List<String> listaFormada = CalcularPagos(propietarioList, costoMensual, metrosPiso);
        //insertamos la lista con el adaptador
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listaFormada);
        //pintamos la lista
        listaView.setAdapter(adaptador);

    }


    /**
     * Metodo para calcular el coste de cada habitacion
     *
     * @param propietarioList lista con los atributos a utilizar como nombre y metros de la hab.
     * @param costoMensual    costo mensual del piso
     * @param metrosPiso      metros cuadrados que posee el piso pueden ser utiles o totales
     * @return regresa una lista de String con todos los precios ya calculados
     */
    public List<String> CalcularPagos(List<Propietario> propietarioList, int costoMensual, int metrosPiso) {
        int valorHabitacion = 0;
        int restante = 0;
        String value;
        List<String> response = new ArrayList<>();
        for (int i = 0; i < propietarioList.size(); i++) {
            Propietario prop = propietarioList.get(i);
            valorHabitacion = (prop.getMetrosCuadrados() * costoMensual) / metrosPiso;
            restante += valorHabitacion;
        }
        Log.d("HappyRoomResponse", "Valor  de todas las habitaciones calculadas");

        restante = (costoMensual - restante) / propietarioList.size();

        Log.d("HappyRoomResponse", "valor de las zonas comunes calculadas");

        for (int i = 0; i < propietarioList.size(); i++) {
            Propietario prop = propietarioList.get(i);
            valorHabitacion = (prop.getMetrosCuadrados() * costoMensual) / metrosPiso;
            valorHabitacion = valorHabitacion + restante;
            value = prop.getNombre() + " Paga: " + valorHabitacion + " Por: " + prop.getMetrosCuadrados() + " Metros Cuadrados";
            response.add(value);
        }

        Log.d("HappyRoomResponse", "valor de cada habitacion con las zonas comunes obtenidas, se regresa la respuesta de cada propietario");
        return response;
    }
}
