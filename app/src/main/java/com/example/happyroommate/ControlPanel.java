package com.example.happyroommate;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.happyroommate.dataBase.InfoLab;
import com.example.happyroommate.entity.Info;

import java.util.List;

public class ControlPanel extends AppCompatActivity implements View.OnClickListener {

    //Variables a utilizar
    TextView nombreP;
    Button btnHR, btnHI, btnHB;

    //Elementos para instanciar la BBDD
    private InfoLab mInfoLab;
    private Info mInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_panel);

        Log.i("Panel de Control", "Navegacion correcta!");

        //Capturando las variables enviadas desde el home
        Bundle extrasIntent = getIntent().getExtras();

        String nombrePrincipal = extrasIntent.getString("nombrePrincipal");

        //Agregamos el nombre para mostrarlo
        nombreP = findViewById(R.id.nombrePersonaP);
        nombreP.setText(nombrePrincipal);

        //Redireccionando a la activity requerida
        btnHR = findViewById(R.id.goHR);
        btnHI = findViewById(R.id.goHI);
        btnHB = findViewById(R.id.goHB);

        btnHR.setOnClickListener(this);
        btnHI.setOnClickListener(this);
        btnHB.setOnClickListener(this);


        //Verificar si en HappyInfo existe data por primera vez
        //Se instancia la BBDD
        mInfoLab = InfoLab.get(this);
        //Data de la BBDD
        List<Info> infos = mInfoLab.getInfos();


        if(infos == null || infos.isEmpty()){
            Info info = new Info();
            info.setNombre("Marta");
            info.setContenido("Mañana ire de compras!");
            info.setColor("#001f33");
            mInfoLab.addInfo(info);

            Info info2 = new Info();
            info2.setNombre("Pedro");
            info2.setContenido("Mañana vendran mis padres y voy a utilizar el salon para cenar con ellos");
            info2.setColor("#001f33");
            mInfoLab.addInfo(info2);
        }


    }

    /*
     * Como tenemos multiples botones
     * y un solo evento onClick podemos
     * verificar el id de cada boton con
     * el id del objeto View que
     * viene de entrada
     */

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.goHR) {
            Log.i("Panel de control", "Navegando a Happy Room");
            //Navegamos al panel de control pasandole el nombre del usuario
            Intent intentHappyRoom = new Intent(this, HappyRoom.class);
            startActivity(intentHappyRoom);
        } else if (v.getId() == R.id.goHI) {
            Log.i("Panel de control", "Navegando a Happy Info");
            //Navegamos al panel de control pasandole el nombre del usuario
            Intent intentHappyRoom = new Intent(this, HappyInfo.class);
            startActivity(intentHappyRoom);
        } else {
            Log.i("Panel de control", "Navegando a Happy Bills");
            //Navegamos al panel de control pasandole el nombre del usuario
            //Intent intentHappyRoom = new Intent(this, HappyRoom.class);
            //startActivity(intentHappyRoom);
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Happy Bill no disponible en esta version!", Toast.LENGTH_LONG);
            toast.show();
        }

    }
}
